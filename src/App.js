import React, {useState, useEffect, useCallback, useRef, Fragment} from 'react';
import {hot} from "react-hot-loader";
import "./style.scss";

const User = (props) => {
    return (
        <div className="user-container">
        <div className="name">{props.name}</div>
            <img src={props.avatarUrl}></img>
        </div>
    )
}

const Command = (props) => {
    const [user, setUser] = useState([]);
    useEffect(() => {
        fetch(`${props.proto}//${props.host}/api/user/${props.obj.added_by}`)
            .then(result => result.json())
            .then((body) => {
                setUser(body);
            })
    }, []);
    let timestamp = new Date(0);
    timestamp.setUTCSeconds(props.obj.time_added);
    return (
        <div className="item">
            <div className="info-container">
                <div className="name">
                    Name: {props.obj.name}
                </div>
                <div className="timestamp">Added: {timestamp.toLocaleString("se-SV")}</div>
                <div className="added-by-container">
                    <div className="text">Added By:</div>
                    <User name={user.username} avatarUrl={user.avatar_url} />
                </div>
            </div>
            <div className="settings-container">
                <div className="count">
                    Played: {props.obj.stats.count}
                </div>
                <div className="volume">
                    Volume: {props.obj.settings.volume}
                </div>
                <div className="download">
                    <a className="link" href={`${props.proto}//${props.host}/media/${props.obj._id.$oid}.mp3`}>Download</a>
                </div>
            </div>
        </div>
    )
}

const Clips = (props) => {
    const [filteredCommands, setFilteredCommands] = useState([]);
    const [clipCount, setClipCount] = useState(0);
    useEffect(() => {
        const filtered = props.clips.filter((element) => {
            if(element.name.search(props.filterText) !== -1) {
                return element;
            };
        });
        setFilteredCommands(filtered);
        props.countSet(filtered.length);
    }, [props.filterText]);
    let command_list = filteredCommands.map((command, index) => {
        return (<Command key={index} proto={props.proto} obj={command} host={props.host} />)
    });
    return (
      <Fragment>
        <div className="command-list">{command_list}</div>
      </Fragment>
    );
}

const SearchBar = (props) => {
    const [searchText, setSearchText] = useState("");
    const change = (event) => {
        setSearchText(event.target.value);
        props.change(event);
    }
    return (
        <div className="search-container">
            <input type="text" placeholder="Search..." onChange={change} value={searchText} />
        </div>
    )
}

const Main = (props) => {
    const guild = window.location.href.split("/")[5];
    const host = window.location.href.split("/")[2];
    const proto = window.location.href.split("/")[0];
    const [clips, setClips] = useState([]);
    const [filterText, setFilterText] = useState(0);
    const [commandCount, setCommandCount] = useState(0);
    useEffect(() => {
        fetch(`${proto}//${host}/api/guild/${guild}`)
            .then(result => result.json())
            .then((body) => {
                setClips(body.commands);
                setFilterText("");
            })
    }, [])
    const search = (event) => {
        setFilterText(event.target.value);
    }

    const countSet = (n) => {
      setCommandCount(n);
    }

    return (
        <Fragment>
          <div className="top-bar">
            <SearchBar change={search}/>
            <div className="count">Commands: {commandCount}</div>
          </div>
            <Clips clips={clips} countSet={countSet} filterText={filterText} proto={proto} host={host}/>
        </Fragment>
    );
}

export default hot(module)(Main);
