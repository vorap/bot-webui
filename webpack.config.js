const path = require("path");
const webpack = require("webpack");

module.exports = {
	entry: ["./src/main.js"],
	mode: "development",
	plugins: [new webpack.HotModuleReplacementPlugin()],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/react'],
				},
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					{
						loader: 'style-loader',
					},
					{
						loader: 'css-loader',
					},
					{
						loader: 'sass-loader',
						options: {
							implementation: require('sass'),
							webpackImporter: false,
							sassOptions: {
								includePaths: ['./node_modules'],
							},
						},
					},
				],
			},
		],
	},
	resolve: {
		extensions: ["*", ".js", ".jsx"],
		alias: {
			'react-dom': '@hot-loader/react-dom',
			'Basic': path.resolve(__dirname, "src/basic/")
		},
	},
	output: {
		path: path.resolve(__dirname, "dist/"),
		publicPath: "/dist/",
		filename: "main.js",
	},
	devServer: {
		contentBase: path.join(__dirname, "public/"),
		port: 3000,
		host: "0.0.0.0",
		publicPath: "http://localhost:3000/dist/",
		hotOnly: true,
	},
}
